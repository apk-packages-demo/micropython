# [micropython](https://pkgs.alpinelinux.org/package/edge/testing/x86_64/micropython) Since Alpine 3.10

Optimised to run on microcontrollers http://micropython.org

Example: https://gitlab.com/alpinelinux-packages-demo/gfortran

* https://en.wikipedia.org/wiki/MicroPython
* https://hub.docker.com/search?q=micropython&type=image&architecture=amd64&operating_system=linux
* https://www.google.com/search?q=MicroPython+site:alpinelinux.org
* https://www.google.com/search?q=MicroPython+site:debian.org
* https://github.com/micropython/micropython
* [*Programming with MicroPython*](https://store.micropython.org/product/BOOKNT-PYBL) - by Nicholas Tollervey
  * https://www.oreilly.com/library/view/programming-with-micropython/9781491972724/
  * https://www.amazon.com/Programming-MicroPython-Embedded-Microcontrollers-Python-ebook/dp/B075X49VVH
  * https://www.worldcat.org/title/programming-with-micropython-embedded-programming-with-microcontrollers-and-python/oclc/1008786679
  * (fr) https://www.worldcat.org/title/programmer-avec-micropython-programmation-python-de-systemes-embarques-a-microcontroleurs/oclc/1039730176
* (fr) https://wiki.mchobby.be/index.php?title=MicroPython-Hack-upip
* https://pypi.org/search/?q=micropython-
* https://github.com/pfalcon/awesome-micropython

## See working version https://gitlab.com/centos-packages-demo/micropython
## See compiled version https://gitlab.com/debian-packages-demo/micropython
